window.onload = () => {
  const resetPokemon = document.getElementById("js--box");
  const pokemon_text = document.getElementsByClassName("pokemon-text");
  const pokemon_image = document.getElementsByClassName("pokemon-picture");

  resetPokemon.onmouseenter = (event) => {
    for (let i = 0; i < pokemon_text.length; i++) {
      let randomPokemonNumber = Math.floor(Math.random() * 151 + 1);
      getPokemon(randomPokemonNumber, pokemon_text[i], pokemon_image[i]);
    }
    resetPokemon.setAttribute("color", "red");
  }

  resetPokemon.onmouseleave = (event) => {
    resetPokemon.setAttribute("color", "green");
  }

  const getPokemon = (numberOfPokemon, pokemonText, pokemonImage) => {
    const BASE_URL = "https://pokeapi.co/api/v2/pokemon/";
    fetch(BASE_URL + numberOfPokemon)
    .then( (data) => {
       return data.json();
    })
    .then( (response) => {
      console.log(response.name);
      changePokemon(response.sprites.front_default, response.name, pokemonImage, pokemonText);
    });

  }
  const changePokemon = (pokemon_sprite, pokemon_name, pokemonImage, pokemonText) => {
    pokemonImage.setAttribute("src", pokemon_sprite);
    pokemonText.setAttribute("value", pokemon_name.toUpperCase());
  }

  //Dit zijn functies om te testen.

}
